# Compositon API 的优势

## Options API 存在的问题

使用传统 Options API 中，新增或者修改一个需求，就需要分别在 data，methods，computed 等地方修改。

![options API](/image/option1.image)

<hr/>

![options API](/image/option2.image)

## Composition API 的优势

可以更加优雅地组织代码、函数，让相关功能的代码更加有序的组织在一起。说白了就是让同一个功能的代码整合到一起，日后修改代码直接找对应的功能模块。

![composition API](/image/composition1.image)

<hr/>

![composition API](/image/composition2.image)
